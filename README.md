# Shopping-Api-Go



## About project

The project is about backend checkout service. The project is built on:
1. Go
2. MongoDB
3. Docker

Reference: [Link](https://gitlab.com/takehometest-munalively/be-01/-/tree/main/01)
### Prerequisite

- [ ] [Docker](https://www.docker.com/)

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Local Setup
1. Clone Repo
2. Run Docker compose
```
docker-compose -p shopping-api-go up --build -d
```
3. Run application to sum total prices

    Here the example:
```
curl http://localhost:8080/product -X POST \
-H "Content-Type: application/json" \
--data '{
"query": "{ totalPrice(name: [\"MacBook Pro\", \"MacBook Pro\"]) }"
}'
```

4. To stop application, run this command
```
docker-compose down -v
```

### Technical Debt
The project doesn't have unit test, because it doesn't separate the repository and service layer so there is a problem with mocking. The unit test will be created as soon as possible.