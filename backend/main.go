package main

import (
	"github.com/go-chi/chi"
	"graphql/infrastructure"
	"graphql/product"
	"log"
	"net/http"
	"net/url"
)

func main() {
	routes := chi.NewRouter()
	r := product.RegisterRoutes(routes)
	log.Println("Serve ready at 8080")
	log.Fatal(http.ListenAndServe(":8080", r))
}

func init() {
	val := url.Values{}
	val.Add("parseTime", "1")
	val.Add("loc", "Asia/Jakarta")

	env := infrastructure.Environment{}
	env.SetEnvironment()
	env.LoadConfig()
	env.InitMongoDB()
}
