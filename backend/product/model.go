package product

type Product struct {
	Sku   string
	Name  string
	Price float64
	Qty   int
}
