package product

import (
	"context"
	"github.com/graphql-go/graphql"
)

var productType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Product",
		Fields: graphql.Fields{
			"sku": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"price": &graphql.Field{
				Type: graphql.Float,
			},
			"qty": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"product": &graphql.Field{
				Type:        productType,
				Description: "Get product by Name",
				Args: graphql.FieldConfigArgument{
					"name": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					var result interface{}
					name, ok := p.Args["name"].(string)
					if ok {
						result = GetProductByName(context.Background(), name)
					}
					return result, nil
				},
			},
			"list": &graphql.Field{
				Type:        graphql.NewList(productType),
				Description: "Get list product",
				Args: graphql.FieldConfigArgument{
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewList(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					var result interface{}
					var listName []string
					for _, name := range p.Args["name"].([]interface{}) {
						listName = append(listName, name.(string))
					}
					result = GetProductList(context.Background(), listName)
					return result, nil
				},
			},

			"totalPrice": &graphql.Field{
				Type:        graphql.Float,
				Description: "Function to sum total price",
				Args: graphql.FieldConfigArgument{
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewList(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					var result interface{}
					var listName []string
					for _, name := range p.Args["name"].([]interface{}) {
						listName = append(listName, name.(string))
					}
					result = SumItem(context.Background(), listName)
					return result, nil
				},
			},
		},
	},
)

var mutationType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"create": &graphql.Field{
				Type:        productType,
				Description: "Create new product",
				Args: graphql.FieldConfigArgument{
					"sku": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"price": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.Float),
					},
					"qty": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.Int),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					product := Product{
						Sku:   p.Args["sku"].(string),
						Name:  p.Args["name"].(string),
						Price: p.Args["price"].(float64),
						Qty:   p.Args["qty"].(int),
					}
					if err := InsertProduct(context.Background(), product); err != nil {
						return nil, err
					}
					return product, nil
				},
			},
			"update": &graphql.Field{
				Type:        productType,
				Description: "Update existing product by name",
				Args: graphql.FieldConfigArgument{
					"sku": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"price": &graphql.ArgumentConfig{
						Type: graphql.Float,
					},
					"qty": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					product := Product{}
					if sku, skuOk := p.Args["sku"].(string); skuOk {
						product.Sku = sku
					}
					if name, nameOk := p.Args["name"].(string); nameOk {
						product.Name = name
					}
					if price, priceOk := p.Args["price"].(float64); priceOk {
						product.Price = price
					}
					if qty, qtyOk := p.Args["qty"].(int); qtyOk {
						product.Qty = qty
					}
					if err := UpdateProduct(context.Background(), product); err != nil {
						return nil, err
					}
					return product, nil
				},
			},
			"delete": &graphql.Field{
				Type:        productType,
				Description: "Delete product by Name",
				Args: graphql.FieldConfigArgument{
					"name": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					name, _ := p.Args["name"].(string)
					if err := DeleteProduct(context.Background(), name); err != nil {
						return nil, err
					}
					return name, nil
				},
			},
		},
	},
)

var Schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query:    queryType,
		Mutation: mutationType,
	},
)
