package product

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"graphql/infrastructure"
	"log"
)

func GetProductByName(ctx context.Context, name string) (result interface{}) {
	var product Product
	data := infrastructure.Mongodb.Collection("productList").FindOne(ctx, bson.M{"Name": name})
	data.Decode(&product)
	log.Println(product)
	return product
}

func GetProductList(ctx context.Context, productNames []string) (result interface{}) {
	var products []Product
	var product Product
	var name string

	for _, name = range productNames {
		data := infrastructure.Mongodb.Collection("productList").FindOne(ctx, bson.M{"Name": name})
		data.Decode(&product)
		products = append(products, product)
	}

	return products
}

func SumItem(ctx context.Context, productNames []string) (result interface{}) {
	var productsQty map[string]int
	var productsPrice map[string]float64
	var product Product
	var name string
	var prices float64

	productsQty = make(map[string]int)
	productsPrice = make(map[string]float64)

	for _, name = range productNames {
		data := infrastructure.Mongodb.Collection("productList").FindOne(ctx, bson.M{"Name": name})
		data.Decode(&product)
		productsQty[name] = productsQty[name] + 1

		if name == "MacBook Pro" && (productsQty[name] <= productsQty["Raspberry Pi B"]) {
			prices = prices + product.Price - productsPrice["Raspberry Pi B"]
		} else if name == "Google Home" && productsQty[name]%3 == 0 {
			continue
		} else if name == "Alexa Speaker" && productsQty[name]%3 == 0 {
			prices = prices - (2 * product.Price) + (3 * product.Price * 0.9)
		} else if name == "Raspberry Pi B" && (productsQty[name] <= productsQty["MacBook Pro"]) {
			continue
		} else if productsQty[name] <= product.Qty {
			productsPrice[product.Name] = product.Price
			prices = prices + product.Price
		} else {
			productsQty[name] = productsQty[name] - 1
			log.Println("product cannot be added")
		}
	}
	log.Println(productsQty)
	return prices
}

func InsertProduct(ctx context.Context, product Product) error {
	_, err := infrastructure.Mongodb.Collection("productList").InsertOne(ctx, product)
	return err
}

func UpdateProduct(ctx context.Context, product Product) error {
	filter := bson.M{"Name": product.Name}
	update := bson.M{"$set": product}
	upsertBool := true
	updateOption := options.UpdateOptions{
		Upsert: &upsertBool,
	}
	_, err := infrastructure.Mongodb.Collection("productList").UpdateOne(ctx, filter, update, &updateOption)
	return err
}

func DeleteProduct(ctx context.Context, name string) error {
	_, err := infrastructure.Mongodb.Collection("productList").DeleteOne(ctx, bson.M{"Name": name})
	return err
}
